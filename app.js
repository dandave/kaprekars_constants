const submit = document.querySelector("#submit");
const input = document.querySelector("#value");
const output = document.querySelector('#output');

let diff;
let ascendingNumber;
let descendingNumber;
let count = 0;
let outputArray = [];

function kaprekar(num) {

    let b = num.split('');

    count++;

    descendingNumber = b.sort((a, b) => { return b - a }).join("");

    ascendingNumber = b.sort((a, b) => { return a - b }).join("");

    diff = descendingNumber - ascendingNumber;

    let result = descendingNumber + ' - ' + ascendingNumber + ' = ' + diff;

    outputArray.push(result);

    while (diff != 6174) {
        // if (diff === 0) {
        //     break;
        // };
        kaprekar(diff.toString());
    }
}


submit.addEventListener('click', () => {

    let inputs = input.value;
    let inputcount = inputs.length;
    let countLength = 0;

    if (!inputs) {
        alert('field must be filled correctly');
        document.getElementById('output').innerHTML = "";
        return;
    }
    if (inputs.length != 4) {
        alert(' only 4 Digits number required');
        document.getElementById('output').innerHTML = "";
        return
    }    
    for (let i in inputs)
    {
        if(inputs.substring(0,1)==inputs[i])
        {
            countLength = countLength + 1;
        }
    }
    if (countLength == inputcount)
    alert("repeated numbers are not allowed");

    kaprekar(inputs);

    output.innerHTML = '';

    let i = 0;

    while (i < outputArray.length) {

        let constants = outputArray[i];
        i++;

        output.innerHTML += `<div> <h4 style="text-align: center">  ${constants} </h4></div>`;
    }

    output.innerHTML += `<hr><div><h4 style="text-align: center"> Number of steps taken  =  ${count} </h4></div><hr>`

    input.value = "";
    count = 0;
    outputArray = [];

});

