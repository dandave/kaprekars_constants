const submit = document.querySelector("#submit");
const input = document.querySelector("#value");
const output = document.querySelector('#output');
 

let diff;
let ascendingNumber;
let descendingNumber;
let count = 0;
let outputArray = [];
output.innerHTML = '';

function clear() {
    input.value = "";
    count = 0;
    outputArray = [];
}

function valueInput() {
    let inputs = input.value;
    if (!inputs) {
        alert('field must be filled');
        document.getElementById('output').innerHTML = "";
        return;
    }
    if (inputs.length != 4) {
        alert(' only 4 Digits number required');
        document.getElementById('output').innerHTML = "";
        return
    }

    kaprekar(inputs.toString());
    // outputs();

    
    let i = 0;

    while ( i < outputArray.length) {
        
        let constants = outputArray[i];
        i++;
        
        output.innerHTML += '<div>' +
            '<h4 style="text-align: center">' + constants + '</h4>' +
            '</div>';
    }

    output.innerHTML += '<hr><div>' +
        '<h4 style="text-align: center"> Number of steps taken  = ' + count + '</h4>' +
        '</div><hr>'
  
        clear();
}


function kaprekar(num) {

    count++;

    let  b= num.split('');

    descendingNumber = b.sort((a,b)=>{return b-a}).join("");

    ascendingNumber = b.sort().join("")

    diff = descendingNumber - ascendingNumber;

    let result = descendingNumber + ' - ' + ascendingNumber + ' = ' + diff;
    outputArray.push(result);
   
    while (diff != 6174) {
        if (diff === 0) {
            break;
        };
        kaprekar(diff.toString());
    }
}

// function outputs() {

  
// }



submit.addEventListener('click', valueInput);

